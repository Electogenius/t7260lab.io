import Head from '/components/head';

export default function NotFound() {
  return (
    <>
    <Head />
    <title>404</title>
    <main>
    <h1>404</h1>
    <h2>yes, you found the 404 page</h2>
    <p>well, you know what to do</p>
    </main>
    </>
  )
}
