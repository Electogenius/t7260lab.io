import Head from '/components/head'
export default function gP({e}) {
  return <>
  <Head />
  <title>cool projects</title>
  <main>
  <h1>Good projects</h1>
  <h2>that you should checkout</h2>
  <li style={{margin: '1em'}}>
  {e.map(v => <li key={v.uuid}><a href={'https://c.gethopscotch.com/p/'+v.uuid}>{v.title}</a></li>)}
  </li>
  </main>
  </>
}

export async function getStaticProps() {
  const a = await fetch('https://api.allorigins.win/raw?url=https://c.gethopscotch.com/api/v2/users/3944888/published_projects?api_token=4f7769439adf7ef8e482d2daef77375cd6d0158b65fcdca543b74b5c0c92&user%5Bauth_token%5D=qKok4lCx8YB9Wgp%2Bo862AA%3D%3D&user%5Bid%5D=4753230');
  const b = await a.json();
  const c = await fetch('https://api.allorigins.win/raw?url=https://community.gethopscotch.com/api/v2/users/5270036/published_projects&api_token=4f7769439adf7ef8e482d2daef77375cd6d0158b65fcdca543b74b5c0c92&user%5Bauth_token%5D=qKok4lCx8YB9Wgp%2Bo862AA%3D%3D&user%5Bid%5D=4753230');
  const d = await c.json();
  var e = [...b.projects, ...d.projects];
  e = e.sort((a, b) => new Date(b.correct_published_at) - new Date(a.correct_published_at));
  return {
    props: {
      e
    }
  }
}
