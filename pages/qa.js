import Head from '/components/head'

export default function Qa() {
  return (
    <>
    <Head />
    <title>q&a</title>
    <main>
    <h1>Q&A</h1>
    <h2>Nothing here</h2>
    <p>If you want to ask a question (you probably don&rsquo;t), then feel free to @ me on the forum.</p>
    </main>
    </>
  )
}
